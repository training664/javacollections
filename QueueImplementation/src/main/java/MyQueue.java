import java.util.NoSuchElementException;

import myCollections.MyLinkedHashMap;
import myCollections.MyLinkedHashMap.Node;

public class MyQueue<E> {

	private int key = 0;
	private MyLinkedHashMap<Integer,E> queue, tempQueue;
	private MyLinkedHashMap<Integer,E> front;
	private MyLinkedHashMap<Integer,E> rear;
	
	public MyQueue(){
		queue = new MyLinkedHashMap<Integer,E>();
		tempQueue = new MyLinkedHashMap<Integer,E>();
		front=rear=null;
	}
	
	
	public void add(E element) {
		queue.put(key, element);
		key++;
	}
	
	public void addFirst(E element) {
		tempQueue.put(key, element);	
		tempQueue.putAll(queue);
		queue = tempQueue;
		key++;
	}
	
	public void addLast(E element) {
		this.add(element);
	}
	
	public boolean contains(E element) {
		return queue.containsValue(element);
	}
	
	public void display() {
		queue.displayElements();
	}
	
	public E getFirst() {
		Node<Integer, E> head = queue.head();
		return head.value;
	}
	
	public E getLast() {
		Node<Integer, E> tail = queue.tail();
		return tail.value;
	}
	
	public E element() {
		return getFirst();
	}
	
	public E peek() {
		if(queue.isEmpty())
			return null;
		return getFirst();
	}
	
	public E peekFirst() {
		if(queue.isEmpty())
			return null;
		return getFirst();
	}
	
	public E peekLast() {
		if(queue.isEmpty())
			return null;
		return getLast();
	}
	
	public E poll() {
		if(queue.isEmpty())
			return null;
		E element = getFirst();
		Node<Integer, E> head = queue.head();
		queue.remove(head.key);
		return element;
	}
	
	public E pollFirst() {
		return poll();
	}
	
	public E pollLast() {
		if(queue.isEmpty())
			return null;
		E element = getLast();
		Node<Integer, E> tail = queue.tail();
		queue.remove(tail.key);
		return element;
	}
	
	public E pop() {
		if(queue.isEmpty())
			throw new NoSuchElementException();
		E element = getLast();
		Node<Integer, E> tail = queue.tail();
		queue.remove(tail.key);
		return element;
	}
	
	public void push(E element) {
		try {
			queue.put(key, element);
			key++;
		}catch(IllegalStateException e) {
			throw e;
		}
	}
	
	public E remove() {
		return poll();
	}
	
	public int size() {
		return queue.size();
	}
	

	
	
	
	
	
	
}
