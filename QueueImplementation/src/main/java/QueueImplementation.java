
public class QueueImplementation {
	
	public static void main(String args[]) {
		
		MyQueue<String> queue = new MyQueue<String>();
		queue.add("R");
		queue.add("S");
		queue.add("T");
		queue.addFirst("P");
		queue.addLast("E");
		queue.addLast("P");
		queue.push("S");
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("Queue contains R: "+ queue.contains("R"));
		System.out.println("Queue contains X: "+ queue.contains("X"));
		System.out.println("...");
		System.out.println("Element(head):" + queue.element());
		System.out.println("getFirst: " + queue.getFirst());
		System.out.println("getLast: " + queue.getLast());
		System.out.println("Peek: " + queue.peek());
		System.out.println("peekFirst: " + queue.peekFirst());
		System.out.println("peekLast: " + queue.peekLast());
		System.out.println("...");
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("poll: " + queue.poll());
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("pollFirst: " + queue.pollFirst());
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("pollLast: " + queue.pollLast());
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("pop: " + queue.pop());
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		System.out.println("remove: " + queue.remove());
		System.out.println("Display Queue: ");
		queue.display();
		System.out.println("...");
		
	}

}
