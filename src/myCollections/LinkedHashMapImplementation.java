package myCollections;
import java.util.Set;

public class LinkedHashMapImplementation {

	public static void main(String args[]) {
		MyLinkedHashMap<String, String> map1 = new MyLinkedHashMap<String, String>();
		System.out.println("Map1 is empty : "+ map1.isEmpty());
		map1.put("2", "R");
		map1.put("3", "F");
		System.out.println("Map1 Contents : ");
		map1.display();
		System.out.println("Map1 is empty : "+ map1.isEmpty());
		//m.clear();
		System.out.println("GET method: " + map1.get("3"));
		map1.put("3", "T");
		System.out.println("Map1 Contents : ");
		map1.display();
		System.out.println("Map1 PutifAbsent Example1 : ");
		map1.putIfAbsent("3", "GH");
		map1.display();
		System.out.println("Map1 PutifAbsent Example1 : ");
		map1.putIfAbsent("13", "GE");
		map1.display();
		map1.put("3", "K");
		map1.put("7", "K");
		map1.put("8", "K");
		map1.put("3", "P");
		System.out.println("Map1 KeySet : "  +map1.keySet());
		System.out.println("Map1 EntrySet : "  +map1.entrySet());
		System.out.println("Map1 Contents Before Remove : ");
		map1.display();
		map1.remove("3");
		System.out.println("Map1 Contents After Remove : ");
		map1.display();
		MyLinkedHashMap<String, String> map2 = new MyLinkedHashMap<String, String>();
		map2.put("34", "G");
		map2.put("7", "F");
		System.out.println("Map2 Contents : ");
		map2.display();
		map2.putAll(map1);
		System.out.println("Map2 Contents After putAll : ");
		map2.display();
		map2.clear();
		System.out.println("Map2 Contents After Clear : ");
		map2.display();
		
		
	}
}
