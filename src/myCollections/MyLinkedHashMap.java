package myCollections;

import java.util.HashSet;
import java.util.Set;

public class MyLinkedHashMap <K,V> { //extends HashMap<K,V> implements Map<K,V>{

	private int initialCapacity = 16;
	
	private int index = 0;
	private Node<K,V>[] map; 
	private Node<K,V> head; 
	private Node<K,V> tail; 
	
    static class Node<K, V> {
        K key;
        V value;
        Node<K,V> next;
        Node<K,V> before,after;
           
        public Node(K key, V value, Node<K,V> next){
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }    
    
	public MyLinkedHashMap(){
		map = new Node[initialCapacity];
	}
	
	public MyLinkedHashMap(int initialCapacity){
		if(initialCapacity<0) {
			throw new IllegalArgumentException();
		}
		map = new Node[initialCapacity];
	}
	 
    private int hash(K key){
        return Math.abs(key.hashCode()) % initialCapacity;
    }
    
	public void put(K key,V value) {
		if(key==null) {
			return;
		}
		int hashIndex=hash(key);
		Node newNode = new Node<K,V>(key, value, null);
		addtoInsertionOrder(newNode);
		if(map[hashIndex]==null) {
			map[hashIndex] = newNode;
		}
		else {
			Node<K,V> prev = null; 
			Node<K,V> curr = map[hashIndex]; 
			while(curr!=null) {
				if(curr.key.equals(key)) {
					if(prev == null) {
						newNode.next = curr.next;
						map[hashIndex] = newNode;
						return;
					}
					else {
						newNode.next = curr.next;
						prev.next = newNode;
					}
				}
				prev = curr;
				curr = curr.next;
			}
			prev.next = newNode;
		}	
	}
	
	public V get(K key) {
		int hashIndex = hash(key);
		if(map[hashIndex] == null) {
			return null;
		}
		else {
			Node<K,V> curr = map[hashIndex];
			while(curr!=null) {
				if(curr.key.equals(key)) {
					return curr.value;
				}
				curr = curr.next;
			}
			return null;
		}
	}
	
	public boolean remove(K key) {
		int hashIndex = hash(key);
		if(map[hashIndex]==null) {
			return false;		// Must Raise Exception
		}
		else {
			Node<K,V> prev = null;
			Node<K,V> curr = map[hashIndex];
			while(curr!=null) {
				if(curr.key.equals(key)) {
					deleteFromInsertionOrder(curr);
					if(prev==null) {
						map[hashIndex] = map[hashIndex].next;
						return true;
					}
					else {
						prev.next = curr.next;
						return true;
					}
				}
				prev = curr;
				curr = curr.next;
			}
			return false;
		}
	}
	
	
	private void addtoInsertionOrder(Node<K, V> newNode) {
		
		if(head==null) {
			head = newNode;
			tail = newNode;
			return;
		}
		
		if(head.key.equals(newNode.key)) {
			deleteFirst();
			insertFirst(newNode);
			return;	
		}
		
		if(tail.key.equals(newNode.key)) {
			deleteLast();
			insertLast(newNode);
			return;
		}
		
		Node<K, V> before = deleteElement(newNode);
		if(before==null) {
			insertLast(newNode);
		}
		else {
			insertAfter(before,newNode);
		}
		
	}
	
	private void deleteFromInsertionOrder(Node<K, V> newNode) {
		if(head.key.equals(newNode.key)) {
			deleteFirst();
			return;
		}
		if(tail.key.equals(newNode.key)) {
			deleteLast();
			return;
		}
		deleteElement(newNode);
	}

	private void insertAfter(MyLinkedHashMap.Node<K, V> before, MyLinkedHashMap.Node<K, V> newNode) {
		// TODO Auto-generated method stub
		Node<K,V> curr = head;
		while(curr!=before) {
			curr = curr.after;
		}
		
		newNode.after = before.after;
		before.after.before = newNode;
		newNode.before = before;
		before.after = newNode;
		
	}

	private MyLinkedHashMap.Node<K, V> deleteElement(MyLinkedHashMap.Node<K, V> newNode) {
		// TODO Auto-generated method stub
		Node<K, V> curr = head;
		while(!curr.key.equals(newNode.key)) {
			if(curr.after==null) {
				return null;
			}
			curr = curr.after;
		}
		
		Node<K,V> before = curr.before;
		curr.before.after = curr.after;
		curr.after.before = curr.before;
		return before;
	}

	private void insertLast(MyLinkedHashMap.Node<K, V> newNode) {
		// TODO Auto-generated method stub
		if(head==null) {
			head = newNode;
			tail = newNode;
			return;
		}
		tail.after = newNode;
		newNode.before = tail;
		tail = newNode;
	}

	private void deleteLast() {
		// TODO Auto-generated method stub
		if(head==tail) {
			head=tail=null;
			return;
		}
		tail = tail.before;
		tail.after = null;
	}

	private void insertFirst(MyLinkedHashMap.Node<K, V> newNode) {
		// TODO Auto-generated method stub
		if(head==null) {
			head=newNode;
			tail=newNode;
			return;
		}
		newNode.after = head;
		head.before = newNode;
		head = newNode;
	}

	private void deleteFirst() {
		// TODO Auto-generated method stub
		if(head==tail) {
			head=tail=null;
			return;
		}
		head = head.after;
		head.before = null;
	}
	
	
	public void display() {
		Node<K, V> curr = head;
		if(curr==null) {
			System.out.println("[]");
		}
		while(curr!=null) {
			System.out.println(curr.key + " " + curr.value);
			curr = curr.after;
		}
	}
		
	
	public Set<String> entrySet() {
		Set<String> entries = new HashSet<String>(); 
		Node<K, V> curr = head;
		while(curr!=null) {
			entries.add("{ " + curr.key + ":" + curr.value + " }" );
			curr = curr.after;
		}
		return entries;
	}
	
	public Set<K> keySet() {
		Set<K> keys = new HashSet<K>(); 
		Node<K, V> curr = head;
		while(curr!=null) {
			keys.add(curr.key);
			curr = curr.after;
		}
		return keys;
	}
	
	public Set<V> values() {
		Set<V> valuesSet = new HashSet<V>(); 
		Node<K, V> curr = head;
		while(curr!=null) {
			valuesSet.add(curr.value);
			curr = curr.after;
		}
		return valuesSet;
	}

	public boolean containsKey(K key) { 
		Node<K, V> curr = head;
		while(curr!=null) {
			if(curr.key.equals(key)){
				return true;
			}
		}
		return false;
	}
	
	public boolean containsValue(V value) {
		Node<K, V> curr = head;
		while(curr!=null) {
			if(curr.value.equals(value)){
				return true;
			}
		}
		return false;
	}
	
	public int size() {
		int length = 0;
		Node<K, V> curr = head;
		while(curr!=null) {
			length++;
			curr = curr.after;
		}
		return length;
	}
	
	public boolean isEmpty() {
		int length = 0;
		Node<K, V> curr = head;
		if(curr==null) {
			return true;
		}
		return false;
	}
	
	public void clear() {
		head=null;
	}
	
	public void putIfAbsent(K key,V value) {
		if(key==null) {
			return;
		}
		int hashIndex=hash(key);
		Node newNode = new Node<K,V>(key, value, null);
		if(map[hashIndex]==null) {
			map[hashIndex] = newNode;
			addtoInsertionOrder(newNode);
		}
		else {
			Node<K,V> prev = null; 
			Node<K,V> curr = map[hashIndex]; 
			while(curr!=null) {
				if(curr.key.equals(key)) {
					return;
				}
				prev = curr;
				curr = curr.next;
			}
			addtoInsertionOrder(newNode);
			prev.next = newNode;
		}	
	}
	
	
	public void putAll(MyLinkedHashMap <K,V> newMap) {
		Node<K, V> newMapNode = newMap.head;
	    while(newMapNode!=null){
	    	K key = newMapNode.key;
	    	V value = newMapNode.value;
	    	if(key==null) {
				return;
			}
			int hashIndex=hash(key);
			Node newNode = new Node<K,V>(key, value, null);
			addtoInsertionOrder(newNode);
			if(map[hashIndex]==null) {
				map[hashIndex] = newNode;
			}
			else {
				Node<K,V> prev = null; 
				Node<K,V> curr = map[hashIndex]; 
				while(curr!=null) {
					if(curr.key.equals(key)) {
						if(prev == null) {
							newNode.next = curr.next;
							map[hashIndex] = newNode;
						}
						else {
							newNode.next = curr.next;
							prev.next = newNode;
						}
					}
					prev = curr;
					curr = curr.next;
				}
				prev.next = newNode;
			}	
	    	newMapNode=newMapNode.after;
	    }
	}
	
	
}








